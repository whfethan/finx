/*>>============================================================================
  The Finx library is distributed under the MIT License (MIT)
    https://opensource.org/licenses/MIT
  See LICENSE or Finx.h for license details.
  Copyright (c) 2016 James Boer.
  Copyright (c) 2022 Weihao Feng.
============================================================================>>*/

#include <chrono>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdexcept>

#include "Finx.h"

using namespace Finx;

#ifndef NDEBUG
#define REQUIRE assert
#else
#define REQUIRE(x) { if (!(x)) throw new std::runtime_error("Failure for condition: " #x); }
#endif

Finx::RuntimePtr TestCreateRuntime()
{
  return Finx::CreateRuntime();
}

Finx::ScriptPtr TestCreateScript(const char * scriptText, Finx::RuntimePtr runtime = nullptr)
{
  if (!runtime)
    runtime = CreateRuntime();

  // Compile the text to bytecode
  auto bytecode = runtime->Compile(scriptText, "Test Script"/*, { "core" }*/);
  if (!bytecode)
    return nullptr;

  // Create a runtime script with the given bytecode
  return runtime->CreateScript(bytecode);
}

Finx::ScriptPtr TestExecuteScript(const char * scriptText, Finx::RuntimePtr runtime = nullptr)
{
  // Create a runtime script
  auto script = TestCreateScript(scriptText, runtime);
  if (!script)
    return nullptr;

  // Execute script until finished
  do
  {
    if (!script->Execute())
      return nullptr;
  }
  while (!script->IsFinished());

  return script;
}

int main(int argc, char ** argv)
{
  printf("Finx version: %s\n", Finx::GetVersionString().c_str());

  GlobalParams params;
  params.logBytecode = true;
  params.logSymbols = true;
  params.errorOnMaxInstructions = false;
  params.maxInstructions = std::numeric_limits<uint32_t>::max();
  Initialize(params);

  const char * scriptText =
    u8R"(

      set a to "Invalid string

    )";

  auto script = TestExecuteScript(scriptText);
  REQUIRE(!script);

  auto memStats = GetMemoryStats();
  printf("\nMemory Stats\n");
  printf("-------------\n");
  printf("Allocation count: %llu\n", memStats.allocationCount);
  printf("Free count: %llu\n", memStats.freeCount);
  printf("Allocated memory: %llu bytes\n", memStats.allocatedMemory);

  return 0;
}
