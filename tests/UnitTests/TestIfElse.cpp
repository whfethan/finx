/*
The Jinx library is distributed under the MIT License (MIT)
https://opensource.org/licenses/MIT
See LICENSE.TXT or Jinx.h for license details.
Copyright (c) 2017 James Boer
*/

#include "UnitTest.h"

using namespace Finx;


TEST_CASE("Test If/Else Branching", "[IfElse]")
{
  SECTION("Test variables and basic statements")
  {
    static const char * scriptText =
      u8R"(

      -- Simple if/else tests

      set a to false.
      if true then
        set a to true;
      end.

      set b to false.
      if true then
        set b to true;
      else
        set b to false;
      end.

      set c to false.
      if false then
        set c to false;
      else
        set c to true;
      end.

      set d to false.
      if false then
        set d to false;
      elsif true then
        set d to true;
      else
        set d to false;
      end.

      set e to false.
      if false then
        set e to false;
      elsif false then
        set e to false;
      elsif true then
        set e to true;
      else
        set e to false;
      end.

      set f to false.
      if false then
        set f to false;
      elsif false then
        set f to false;
      elsif false then
        set f to false;
      else
        set f to true;
      end.

      set g to false.
      if true then
        if true then
          set g to true;
        end;
      end.

      set h to false.
      if false then
      else
        if true then
          set h to true;
        end;
      end.

      )";

    auto script = TestExecuteScript(scriptText);
    REQUIRE(script);
    REQUIRE(script->GetVariable("a") == true);
    REQUIRE(script->GetVariable("b") == true);
    REQUIRE(script->GetVariable("c") == true);
    REQUIRE(script->GetVariable("d") == true);
    REQUIRE(script->GetVariable("e") == true);
    REQUIRE(script->GetVariable("f") == true);
    REQUIRE(script->GetVariable("g") == true);
    REQUIRE(script->GetVariable("h") == true);
  }

  SECTION("Test non-standard conditional tests")
  {
    static const char * scriptText =
      u8R"(

      set a to 3.
      if a then
        set a to 4;
      end.

      set b to [].
      if not b then
        set b to "check";
      end.

      set c to null.
      if c then
        set c to 123;
      end.

      )";

    auto script = TestExecuteScript(scriptText);
    REQUIRE(script);
    REQUIRE(script->GetVariable("a") == 4);
    REQUIRE(script->GetVariable("b") == "check");
    REQUIRE(script->GetVariable("c") == nullptr);
  }
}