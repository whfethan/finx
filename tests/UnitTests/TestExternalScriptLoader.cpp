/*>>===========================================================================
  The Finx library is distributed under the MIT License (MIT)
    https://opensource.org/licenses/MIT
  See LICENSE or Finx.h for license details.
  Copyright (c) 2022 Weihao Feng.
===========================================================================>>*/

#include "UnitTest.h"

using namespace Finx;

#define EXTERNAL_LOADER(name) auto name = [&](const String& lib, Any loaderArg, BufferPtr content, std::string* err) -> ExternalLoaderResult

TEST_CASE("Test External Script Loader", "[ExternalLoader]")
{
  SECTION("Test loading external script without an external loader configured")
  {
    const char* scriptText = u8R"(
      import external "test/does_not_exist".

      testfunc.
    )";
    auto runtime = CreateRuntime();
    auto script = TestExecuteScript(scriptText, runtime);
    REQUIRE(!script);
  }

  SECTION("Test external script loader argument passing")
  {
    const char* scriptText = u8R"(

      import external "test/script1".
      import external "example.finx".
      import external system.

    )";
    std::vector<std::string> requestedPaths;
    EXTERNAL_LOADER(loader)
    {
      requestedPaths.emplace_back(std::string(lib));
      return ExternalLoaderResult::SourceFile;
    };
    auto runtime = CreateRuntime(0, loader);
    auto script = TestExecuteScript(scriptText, runtime);
    REQUIRE(script);
    REQUIRE(requestedPaths.size() == 3);
    REQUIRE(requestedPaths[0] == "test/script1");
    REQUIRE(requestedPaths[1] == "example.finx");
    REQUIRE(requestedPaths[2] == "system");
  }

  SECTION("Test access to external script")
  {
    const char* scriptText = u8R"(

      import external "test_script".

      set a to testfunc.
      set c to b.
    )";
    const char* libScriptText = u8R"(

      -- Export library function.
      public function testfunc begin
        return 8964;
      end.

      -- Export property
      set public b to 1234.
    )";
    EXTERNAL_LOADER(loader)
    {
      if (lib == "test_script") {
        content->Write(libScriptText, strlen(libScriptText) + 1);
        return ExternalLoaderResult::SourceFile;
      }
      return ExternalLoaderResult::Failed;
    };
    auto runtime = CreateRuntime(0, loader);
    auto script = TestExecuteScript(scriptText, runtime);
    REQUIRE(script);
    REQUIRE(script->GetVariable("a") == 8964);
    REQUIRE(script->GetVariable("c") == 1234);
  }

  SECTION("Test call function from external library through alias")
  {
    const char* libScriptText = u8R"(

      library system:

      public function do something {x}
      begin
        return x + 3;
      end.
    )";
    const char* scriptText = u8R"(

      import core.
      import external system as sys.

      set a to sys do something 3.
    )";
    EXTERNAL_LOADER(loader)
    {
      if (lib == "system") {
        content->Write(libScriptText, strlen(libScriptText) + 1);
        return ExternalLoaderResult::SourceFile;
      }
      return ExternalLoaderResult::Failed;
    };
    auto runtime = CreateRuntime(0, loader);
    auto script = TestExecuteScript(scriptText, runtime);
    REQUIRE(script);
    REQUIRE(script->GetVariable("a") == 6);
  }

  SECTION("Test redundant import statements over the same library")
  {
    const char* scriptText = u8R"(

      import external system.
      import external system2.
      import external system.
      import external system.
      import external system2.
    )";
    std::map<String, int> requestRecords;
    EXTERNAL_LOADER(loader)
    {
      if (requestRecords.find(lib) == requestRecords.end())
        requestRecords[lib] = 1;
      else
        requestRecords[lib]++;
      return ExternalLoaderResult::SourceFile;
    };
    auto runtime = CreateRuntime(0, loader);
    auto script = TestExecuteScript(scriptText, runtime);
    REQUIRE(script);
    REQUIRE(requestRecords["system"] == 1);
    REQUIRE(requestRecords["system2"] == 1);
  }
}
