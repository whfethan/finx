/*>>===========================================================================
  The Finx library is distributed under the MIT License (MIT)
    https://opensource.org/licenses/MIT
  See LICENSE or Finx.h for license details.
  Copyright (c) 2017 James Boer.
  Copyright (c) 2022 Weihao Feng.
===========================================================================>>*/

#include "UnitTest.h"

using namespace Finx;

static bool s_initializedGlobals = false;

Finx::RuntimePtr TestCreateRuntime()
{
  if (!s_initializedGlobals)
  {
    GlobalParams globalParams;
    globalParams.enableLogging = true;
    globalParams.logBytecode = false;
    //globalParams.logFn = [] (LogLevel, const char *s) { std::cout << s; };
    Finx::Initialize(globalParams);

    s_initializedGlobals = true;
  }

  return Finx::CreateRuntime();
}

ScriptPtr TestCreateScript(const char * scriptText, Finx::RuntimePtr runtime, FinxAny userContext)
{
  if (!runtime)
    runtime = TestCreateRuntime();

  // Compile the script text to bytecode
  auto bytecode = runtime->Compile(scriptText);
  if (!bytecode)
    return nullptr;

  // Create a script with the compiled bytecode
  return runtime->CreateScript(bytecode, userContext);
}

Finx::ScriptPtr TestExecuteScript(const char * scriptText, Finx::RuntimePtr runtime, FinxAny userContext)
{
  if (!runtime)
    runtime = TestCreateRuntime();

  // Compile the script text to bytecode
  auto bytecode = runtime->Compile(scriptText);
  if (!bytecode)
    return nullptr;

  // Create a script with the compiled bytecode
  auto script = runtime->CreateScript(bytecode, userContext);

  // Execute script until finished
  if (!script->Execute(true))
    return nullptr;

  return script;
}