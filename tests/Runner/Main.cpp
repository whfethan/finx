#include <ios>
#include <iostream>
#include <fstream>
#include "Finx.h"

using namespace Finx;

ScriptPtr TestExecuteScript(const char * scriptText, RuntimePtr runtime, FinxAny userContext)
{
  // Compile the script text to bytecode
  auto bytecode = runtime->Compile(scriptText);
  if (!bytecode)
    return nullptr;

  // Create a script with the compiled bytecode
  auto script = runtime->CreateScript(bytecode, userContext);

  // Execute script until finished
#if 0
  do
  {
    if (!script->Execute())
      return nullptr;
  }
  while (!script->IsFinished());
#endif
  if (!script->Execute(true))
    return nullptr;

  return script;
}

#define EXTERNAL_LOADER(name) auto name = [&](const String& lib, Any loaderArg, BufferPtr content, std::string* err) -> ExternalLoaderResult

int main(int argc, char** argv)
{
  GlobalParams params;
  params.enableDebugInfo = true;
  params.logBytecode = true;
  params.enableLogging = true;
  params.logFn = [](LogLevel, const char* s) { std::cout << s; };

  if (argc < 2)
    return 0;

  std::ifstream iFile;
  iFile.open(argv[1], std::ios::in | std::ios::ate);
  if (!iFile.is_open())
  {
    std::cout << "Cannot open file: " << argv[1] << std::endl;
    return -1;
  }
  std::streampos size = iFile.tellg();
  if (size == 0)
    return 0;
  char* content = new char [size];
  iFile.seekg(0, std::ios::beg);
  iFile.read(content, size);
  iFile.close();

  Initialize(params);
  int res = 1;
  EXTERNAL_LOADER(loader) {
    std::cout << "Requested: " << lib << std::endl;
    return ExternalLoaderResult::SourceFile;
  };
  auto runtime = CreateRuntime(0, loader);

  auto script = runtime->CreateScript(content);
  delete[] content;
  if (script) {
    res = !script->Execute(true /*runUntilFinish*/);
  }

  return res;
}
