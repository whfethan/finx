/*>>============================================================================
  The Finx library is distributed under the MIT License (MIT)
    https://opensource.org/licenses/MIT
  See LICENSE or Finx.h for license details.
  Copyright (c) 2016 James Boer.
  Copyright (c) 2022 Weihao Feng.
============================================================================>>*/

#pragma once
#ifndef FX_BUFFER_H
#define FX_BUFFER_H

#include "Finx/_FxMemory.h"

/*! \file */

/*! \namespace */
namespace Finx {

/// Buffer Interface
#if 0
class IReadOnlyBuffer {
public:
  [[nodiscard]] virtual void* Data(size_t pos) const = 0;
  [[nodiscard]] virtual size_t Size() const = 0;
  virtual void Read(size_t* pos, void* buffer, size_t bytes) = 0;

protected:
  virtual ~IReadOnlyBuffer() {};
};

class IBuffer : public IReadOnlyBuffer {
public:
  virtual void Clear() = 0;
  virtual void Reserve(size_t bytes) = 0;
  [[nodiscard]] virtual size_t Capacity() const = 0;

  virtual void Write(size_t* pos, const void* data, size_t bytes) = 0;
  virtual void Write(const void* data, size_t bytes) = 0;

protected:
  virtual ~IBuffer() {}
};

/// Real buffer implementations

class ExternalBuffer final : virtual IReadOnlyBuffer {
public:
  ExternalBuffer(void* data, size_t bytes)
  {
    assert(data != nullptr && bytes);
    m_data = static_cast<char*>(data);
    m_size = bytes;
  }

  [[nodiscard]] size_t Size() const override { return m_size; }
  [[nodiscard]] void* Data(size_t pos) const override {
    assert(pos <= m_size);
    return m_data + pos;
  }

  void Read(size_t* pos, void* buffer, size_t bytes) override {
    assert(buffer != nullptr && bytes <= (m_size - *pos));
    ::memmove(buffer, m_data + *pos, bytes);
    *pos += bytes;
  }

private:
  char* m_data;
  size_t m_size;
};

class Buffer final : public IBuffer {
public:
  Buffer() : m_data(nullptr), m_size(0), m_capacity(0) {}
  ~Buffer() {
    if (m_data)
      MemFree(m_data, m_capacity);
  };

  [[nodiscard]] size_t Size() const override { return m_size; }
  [[nodiscard]] void* Data(size_t pos) const override {
    assert(pos <= m_size);
    return m_data + pos;
  }

  void Clear() override {
    m_size = 0;
    if (m_data) m_data[0] = 0;
  }
  void Reserve(size_t bytes) override {
    if (bytes <= m_capacity)
      return;
    m_data = static_cast<char*>(MemReallocate(m_data, bytes, m_capacity));
    m_capacity = bytes;
  }
  [[nodiscard]] size_t Capacity() const override { return m_capacity; }

  template <typename T> void Read(size_t* pos, T* data) {
    assert(sizeof(T) <= (m_size - *pos));
    *data = reinterpret_cast<T>(m_data + *pos);
    *pos += sizeof(T);
  }
  void Read(size_t* pos, void* buffer, size_t bytes) override {
    assert(buffer != nullptr && bytes <= (m_size - *pos));
    ::memmove(buffer, m_data + *pos, bytes);
    *pos += bytes;
  }

  void Write(size_t* pos, const void* data, size_t bytes) override {
    assert(data != nullptr && bytes);
    if (m_capacity < (*pos + bytes))
      Reserve(m_capacity + bytes + (m_capacity >> 1));
    ::memmove(m_data + *pos, data, bytes);
    *pos += bytes;
    m_data[*pos] = 0;
    if (m_size < *pos)
      m_size = *pos;
  }

  void Write(const void* data, size_t bytes) override {
    assert(data && bytes);
    if (m_capacity < bytes)
      Reserve((m_capacity + bytes) + (m_capacity >> 1));
    ::memmove(m_data, data, bytes);
    m_size = bytes;
  }

private:
  char* m_data;
  size_t m_size;
  size_t m_capacity;
};

using BufferPtr = std::shared_ptr<Buffer>;
using ROBufferPtr = std::shared_ptr<IReadOnlyBuffer>;
#endif

class Buffer;

using BufferPtr = std::shared_ptr<Buffer>;

class Buffer {
public:
  Buffer();
  ~Buffer();
  size_t Capacity() const;
  void Clear();
  template <typename T>
  void Read(size_t* pos, T data)
  {
    assert(sizeof(*data) <= (m_size - *pos));
    *data = *reinterpret_cast<T>(m_data + *pos);
    *pos += sizeof(*data);
  }
  void Read(size_t* pos, void* data, size_t bytes);
  void Read(size_t* pos, BufferPtr& buffer, size_t bytes);
  void Reserve(size_t size);
  inline uint8_t* Data(size_t pos) const { return m_data + pos; }
  inline size_t Size() const { return m_size; }
  void Write(const void* data, size_t bytes);
  void Write(size_t* pos, const void* data, size_t bytes);

private:
  uint8_t* m_data;
  size_t m_size;
  size_t m_capacity;
};

BufferPtr CreateBuffer();

} // namespace Finx

#endif // FX_BUFFER_H
