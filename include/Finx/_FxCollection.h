/*>>============================================================================
  The Finx library is distributed under the MIT License (MIT)
    https://opensource.org/licenses/MIT
  See LICENSE or Finx.h for license details.
  Copyright (c) 2016 James Boer.
  Copyright (c) 2022 Weihao Feng.
============================================================================>>*/

#pragma once
#ifndef FX_COLLECTION_H
#define FX_COLLECTION_H

#include <map>
#include "Finx/_FxMemory.h"

/*! \file */

/*! \namespace */
namespace Finx {

class Variant;

using Collection = std::map<Variant, Variant, std::less<Variant>, Allocator<std::pair<const Variant, Variant>>>;
using CollectionPtr = std::shared_ptr<Collection>;
using CollectionItr = Collection::iterator;
using CollectionItrPair = std::pair<CollectionItr, CollectionPtr>;

CollectionPtr CreateCollection();

} // namespace Finx

#endif // FX_COLLECTION_H

