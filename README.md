# Finx  [![pipeline status](https://gitlab.com/whfethan/finx/badges/master/pipeline.svg)](https://gitlab.com/whfethan/finx/-/commits/master) [![coverage report](https://gitlab.com/whfethan/finx/badges/main/coverage.svg)](https://gitlab.com/whfethan/finx/-/commits/main) [![Latest Release](https://gitlab.com/whfethan/finx/-/badges/release.svg)](https://gitlab.com/whfethan/finx/-/releases)

*Copyright © 2016 James Boer. All rights reserved.*</br>
*Copyright © 2022 Weihao Feng. All rights reserved.*


Finx was originated from [Jinx](https://github.com/JamesBoer/Jinx). A small embeddable scripting language engine that is
intended to be integrated into bigger applications.

## 0. Overview.

**Finx is intended to ...**

* ***be close to natural language:***
    * A top level statement is terminated by full stop `.` and statements inside a block statement are separated by `;`.
    * Keywords and identifiers are case-insensitive.
    * Function signature looks like natural language.
    * ..., and much more!
* ***be modular:***
    * Finx provides built-in module management which allows the script import libraries pre-defined by the host application
      or external script through external script loader callback.
* ***be small and simple:***
    * A static linked Finx interpreter (Compiler + Runtime) takes ~390 Kilobytes.
    * Intuitive APIs and easy embedding workflow (see below).
    * The source files are fairly easy to read and understand, which allows developers to do extra modifications.
    * Stack-based virtual machine and single pass parser & compiler.
* ***be thread-safe:***
    * One runtime object can be shared among threads to run multiple scripts in parallel.
* ***be portable:***
    * The library supports Windows 10, macOS 10.14+, Linux, *BSD and any other operating systems that ship with
      full c++17 compliant compiler and c++ standard library.
    * Compiled Finx bytecodes can be saved for use in the future as the binary format is self-contained.
* ***be free, flexible, and safe:***
    * The runtime provides only essential functions needed for core language features.
    * Untrusted by design: VM and runtime have no access to the filesystem and even memory (with proper configuration).
    * Anything that can be declared by a Finx script can be declared by the host application through runtime/script api.
    * Finx **does not** perform integrity checks on loaded bytecode stream. It is host applications' job to prevent
      malformed or corrupted data from being fed to the runtime. This grants developers freedom to integrate Finx bytecode into
      their own binary format.
    * Licensed under permissive MIT licence.
* ***keep evolving:***
    * Suggestions, improvements, criticisms are always appreciated.

**However, Finx is not intended to ...**

* **be performant:**
    * A stack-based vm means the maximal performance could be very limited compared to a register-based one. It is
      possible to implement an optimizer, but this will add another layer of code complexity. Therefore, it is not recommended
      to use Finx under performance critical context.
* **be a replacement to any of other scripting langauges:**
    * Finx will not implement fancy features like class and handles, as they can break the simplicity of the language.
    * Finx focuses on expression clarity, language simplicity, general stability and data passing between the vm and
      the host application.

## 1. Getting Started with Finx.

Finx requires a C++17 compliant compiler to build.

Finx supports the CMake build system, so for CMake users, adding Finx should be as simple as pointing your build scripts
to the root folder of the project.

## 2. Run Finx in 3 Steps.

```c++
#include <iostream>
#include <Finx.h>

using namespace Finx;

int main(void)
{
    const char* scriptText = u8R"(

    import core.

    function say {x} to the world
    begin
        if x's type = string then
            write line x + ", world!";
        else
            write line "I don't know what to say!";
        end;
    end.

    Say "Hello" to the world. 

    )";
    // Step 1: Initialize Finx global parameters (optional).
    GlobalParams params;
    // "write line" won't output anything unless this enabled.
    params.enableLogging = true;
    params.logFn = [](LogLevel, const char* s) { std::cout << s; };
    // Initialize the Finx library with these parameters.
    Initialize(params);

    // Step 2: Create Finx runtime.
    auto runtime = CreateRuntime();
    
    // Step 3: Compile and execute the script in one function call.
    auto script = runtime->ExecuteScript(scriptText);
    
    return !script->IsFinished();
}
```

## 3. Current status

Although Finx was forked from the stable version of Jinx, it is still in alpha stage. More tests are needed and
the document is still incomplete. API might change from time to time.

| ITEM                              | STATUS           |
|-----------------------------------|------------------|
| Main library                      | Finished (Alpha) |
| Finx Interpreter/Compiler (Finxc) | In Progress      |
| Complete Document                 | In Progress      |
| Gitlab CI Config                  | In Progress      |