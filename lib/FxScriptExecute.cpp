/*>>============================================================================
  The Finx library is distributed under the MIT License (MIT)
    https://opensource.org/licenses/MIT
  See LICENSE or Finx.h for license details.
  Copyright (c) 2022 Weihao Feng.
============================================================================>>*/

#include "FxScript.h"
#include "FxInternal.h"

namespace Finx::Impl {

bool OptimizedScriptRunner::Execute(Script* s, bool runUntilFinish)
{
  Script::ExecutionFrame* frame;
  BinaryReader* br;
  auto runtime = s->m_runtime;
  auto & stack = s->m_stack;
  uint8_t inst;

  // Error checks and state reset.
  if (s->m_error)
    return false;
  else if (!s->m_execution.back().bytecode) {
    s->Error("No bytecode to execute");
    return false;
  } else if (s->m_finished) {
    assert(s->m_execution.size() == 1);
    s->m_execution.back().reader.Seek(s->m_bytecodeStart);
    s->m_finished = false;
  }

  #define CASE_INST(C)  __4D12A2AB_ ## C
  static const void* __DispatchTable[(uint8_t)Opcode::NumOpcodes] = {
    #define OPCODE(C, _) &&__4D12A2AB_ ## C,
    #include "FxOpcodes.h"

#undef OPCODE
  };

  #define DISPATCH_DIRECT()   goto *__DispatchTable[inst]
  #define DISPATCH()          br->Read(&inst); tickInstCount++; \
                              DISPATCH_DIRECT()
  #define POP()               s->Pop()
  #define PUSH(arg)           s->Push(arg)
  #define ERROR(arg)          s->Error(arg)
  #define READ(...)           br->Read(__VA_ARGS__)
  #define UPDATE_FRAME()      frame = &s->m_execution.back(); br = &frame->reader

  // Mark script execution start time
  auto begin = std::chrono::high_resolution_clock::now();
  uint32_t tickInstCount = 0;

__JINX_INTERP_NEXT_FRAME:
  UPDATE_FRAME();

  // Start loop
  DISPATCH(); {
    CASE_INST(Add): {
      auto op2 = POP();
      auto op1 = POP();
      auto result = op1 + op2;
      if (result.IsNull()) {
        ERROR("Invalid variable for addition");
        return false;
      }
      PUSH(result);
    }
    DISPATCH();

    CASE_INST(And): {
      auto op2 = POP();
      auto op1 = POP();
      PUSH(op1.GetBoolean() && op2.GetBoolean());
    }
    DISPATCH();

    CASE_INST(CallFunc): {
      RuntimeID id;
      READ(&id, sizeof(id));
      FunctionDefinitionPtr functionDef = runtime->FindFunction(id);
      if (!functionDef)
      {
        ERROR("Could not find function definition");
        return false;
      }
      // Check to see if this is a bytecode function
      if (functionDef->GetBytecode())
      {
        s->CallBytecodeFunction(functionDef, Script::OnReturn::Continue);
        UPDATE_FRAME();
        if (s->m_error)
          return false;
      }
      // Otherwise, call a native function callback
      else if (functionDef->GetCallback())
      {
        PUSH(s->CallNativeFunction(functionDef));
        UPDATE_FRAME();
        if (s->m_error)
          return false;
      }
      else
      {
        ERROR("Error in function definition");
        return false;
      }
    }
    DISPATCH();

    CASE_INST(Cast): {
      uint8_t b;
      READ(&b);
      auto type = ByteToValueType(b);
      assert(!stack.empty());
      stack.back().ConvertTo(type);
    }
    DISPATCH();

    CASE_INST(Decrement): {
      auto op1 = POP();
      auto op2 = POP();
      if (!op1.IsNumericType())
      {
        ERROR("Can't decrease non-numeric type");
        return false;
      }
      else if (!op2.IsNumericType())
      {
        ERROR("Can't decrease value by a non-numeric type");
        return false;
      }
      op2 -= op1;
      PUSH(op2);
    }
    DISPATCH();

    CASE_INST(Divide): {
      auto op2 = POP();
      auto op1 = POP();
      if (op2.GetNumber() == 0.0)
      {
        ERROR("Divide by zero");
        return false;
      }
      auto result = op1 / op2;
      if (result.IsNull())
      {
        ERROR("Invalid variable for division");
        return false;
      }
      PUSH(result);
    }
    DISPATCH();

    CASE_INST(Equals): {
      auto op2 = POP();
      auto op1 = POP();
      PUSH(op1 == op2);
    }
    DISPATCH();

    CASE_INST(EraseItr): {
      RuntimeID id;
      READ(&id);
      auto var = s->GetVariable(id);
      if (!var.IsCollectionItr())
      {
        ERROR("Expected collection iterator for this form of erase");
        return false;
      }
      auto itr = var.GetCollectionItr().first;
      auto coll = var.GetCollectionItr().second;
      if (itr != coll->end())
        itr = coll->erase(itr);
      s->SetVariable(id, std::make_pair(itr, coll));
    }
    DISPATCH();

    CASE_INST(ErasePropKeyVal): {
      uint32_t subscripts;
      READ(&subscripts);
      RuntimeID propId;
      READ(&propId);

      bool success = runtime->SetProperty(propId, [s, subscripts](Variant& coll)->bool
      {
        if (!coll.IsCollection())
        {
          s->Error("Expected collection when accessing by key");
          return false;
        }
        auto collection = coll.GetCollection();

        // Find the appropriate collection-key pair
        auto pair = s->WalkSubscripts(subscripts, collection);
        if (pair.first == nullptr)
        {
          s->Error("Could not find collection to erase");
          return false;
        }
        collection = pair.first;
        Variant key = pair.second;

        // Erase the value based on the key if it exists
        auto itr = collection->find(key);
        if (itr != collection->end())
          collection->erase(itr);
        return true;
      });
      if (!success)
        return false;
    }
    DISPATCH();

    CASE_INST(EraseVarKeyVal): {
      uint32_t subscripts;
      READ(&subscripts);
      RuntimeID id;
      READ(&id);
      Variant coll = s->GetVariable(id);
      if (!coll.IsCollection())
      {
        ERROR("Expected collection when accessing by key");
        return false;
      }
      auto collection = coll.GetCollection();

      // Find the appropriate collection-key pair
      auto pair = s->WalkSubscripts(subscripts, collection);
      if (pair.first == nullptr)
        return false;
      collection = pair.first;
      Variant key = pair.second;

      // Erase the value based on the key if it exists
      auto itr = collection->find(key);
      if (itr != collection->end())
        collection->erase(itr);
    }
    DISPATCH();

    CASE_INST(Exit):
      while (s->m_execution.size() > 1)
        s->m_execution.pop_back();
      s->m_finished = true;
      goto __JINX_INTERP_EXIT;

    CASE_INST(Function): {
      FunctionSignature signature;
      signature.Read(*br);
      if (signature.GetVisibility() != VisibilityType::Local)
        s->m_library->RegisterFunctionSignature(signature);
      else
        s->m_localFunctions.push_back(signature.GetId());
      // Note: we add 5 bytes to the current position to skip over the jump command and offset value.
      runtime->RegisterFunction(signature, frame->bytecode, br->Tell() + 5);
    }
    DISPATCH();

    CASE_INST(Greater): {
      auto op2 = POP();
      auto op1 = POP();
      if (!ValidateValueComparison(op1, op2))
      {
        ERROR("Incompatible types in operator >");
        return false;
      }
      auto result = op1 > op2;
      PUSH(result);
    }
    DISPATCH();

    CASE_INST(GreaterEq): {
      auto op2 = POP();
      auto op1 = POP();
      if (!ValidateValueComparison(op1, op2))
      {
        ERROR("Incompatible types in operator >");
        return false;
      }
      auto result = op1 >= op2;
      PUSH(result);
    }
    DISPATCH();

    CASE_INST(Increment): {
      auto op1 = POP();
      auto op2 = POP();
      if (!op1.IsNumericType())
      {
        ERROR("Can't increase non-numeric type");
        return false;
      }
      else if (!op2.IsNumericType())
      {
        ERROR("Can't increase value by a non-numeric type");
        return false;
      }
      op2 += op1;
      PUSH(op2);
    }
    DISPATCH();

    CASE_INST(Jump): {
      uint32_t jumpIndex;
      READ(&jumpIndex);
      br->Seek(jumpIndex);
    }
    DISPATCH();

    CASE_INST(JumpFalse): {
      uint32_t jumpIndex;
      READ(&jumpIndex);
      if (POP().GetBoolean() == false)
        br->Seek(jumpIndex);
    }
    DISPATCH();

    CASE_INST(JumpFalseCheck): {
      uint32_t jumpIndex;
      READ(&jumpIndex);
      if (stack.empty())
      {
        ERROR("Stack underflow");
        return false;
      }
      const auto & op1 = stack.back();
      if (!op1.GetBoolean())
      {
        br->Seek(jumpIndex);
      }
    }
    DISPATCH();

    CASE_INST(JumpTrue): {
      uint32_t jumpIndex;
      READ(&jumpIndex);
      if (POP().GetBoolean() == true)
        br->Seek(jumpIndex);
    }
    DISPATCH();

    CASE_INST(JumpTrueCheck): {
      uint32_t jumpIndex;
      READ(&jumpIndex);
      if (stack.empty())
      {
        ERROR("Stack underflow");
        return false;
      }
      const auto & op1 = stack.back();
      if (op1.GetBoolean())
      {
        br->Seek(jumpIndex);
      }
    }
    DISPATCH();

  CASE_INST(Import): {
    String libPath;
    READ(&libPath);
    std::string error;
    ScriptIPtr script;
    if(!runtime->LibraryExists(libPath) && !runtime->LoadExternalScript(&script, libPath, &error))
    {
      ERROR("Failed to import external library");
      return false;
    }
  }
  DISPATCH();

    CASE_INST(Less): {
      auto op2 = POP();
      auto op1 = POP();
      if (!ValidateValueComparison(op1, op2))
      {
        ERROR("Incompatible types in operator <");
        return false;
      }
      auto result = op1 < op2;
      PUSH(result);
    }
    DISPATCH();

    CASE_INST(LessEq): {
      auto op2 = POP();
      auto op1 = POP();
      if (!ValidateValueComparison(op1, op2))
      {
        ERROR("Incompatible types in operator <=");
        return false;
      }
      auto result = op1 <= op2;
      PUSH(result);
    }
    DISPATCH();

    CASE_INST(Library): {
      String libraryName;
      READ(&libraryName);
      s->m_library = runtime->GetLibraryInternal(libraryName);
    }
    DISPATCH();

    CASE_INST(LoopCount): {
      assert(stack.size() >= 3);
      auto top = stack.size() - 1;
      auto loopVal = stack[top - 2];
      auto loopDest = stack[top - 1];
      auto incBy = stack[top];
      if (incBy.IsNull())
      {
        if (loopVal > loopDest)
          incBy = -1;
        else
          incBy = 1;
      }
      loopVal += incBy;
      stack[top - 2] = loopVal;
      auto incVal = incBy.GetNumber();
      if (incVal > 0)
        PUSH(loopVal <= loopDest);
      else if (incVal < 0)
        PUSH(loopVal >= loopDest);
      else
      {
        ERROR("Incremented loop counter by zero");
        return false;
      }
    }
    DISPATCH();

    CASE_INST(LoopOver): {
      assert(stack.size() >= 2);
      auto top = stack.size() - 1;
      auto itr = stack[top];
      assert(itr.IsCollectionItr());
      auto coll = stack[top - 1];
      assert(coll.IsCollection() && coll.GetCollection());
      bool finished = itr.GetCollectionItr().first == coll.GetCollection()->end();
      if (!finished)
      {
        ++itr;
        finished = itr.GetCollectionItr().first == coll.GetCollection()->end();
      }
      stack[top] = itr;
      PUSH(finished);
    }
    DISPATCH();

    CASE_INST(Mod): {
      auto op2 = POP();
      auto op1 = POP();
      if (op2.GetNumber() == 0.0)
      {
        ERROR("Mod by zero");
        return false;
      }
      auto result = op1 % op2;
      if (result.IsNull())
      {
        ERROR("Invalid variable for mod");
        return false;
      }
      PUSH(result);
    }
    DISPATCH();

    CASE_INST(Multiply): {
      auto op2 = POP();
      auto op1 = POP();
      auto result = op1 * op2;
      if (result.IsNull())
      {
        ERROR("Invalid variable for multiply");
        return false;
      }
      PUSH(result);
    }
    DISPATCH();

    CASE_INST(Negate): {
      auto op1 = POP();
      if (!op1.IsNumericType())
      {
        ERROR("Only numeric types can be negated");
        return false;
      }
      auto result = op1 * -1;
      PUSH(result);
    }
    DISPATCH();

    CASE_INST(Not):
      PUSH(!POP().GetBoolean());
      DISPATCH();

    CASE_INST(NotEquals): {
      auto op2 = POP();
      auto op1 = POP();
      auto result = op1 != op2;
      PUSH(result);
    }
    DISPATCH();

    CASE_INST(Or): {
      auto op2 = POP();
      auto op1 = POP();
      PUSH(op1.GetBoolean() || op2.GetBoolean());
    }
    DISPATCH();

    CASE_INST(Pop):
      POP();
      DISPATCH();

    CASE_INST(PopCount): {
      uint32_t count;
      READ(&count);
      for (uint32_t i = 0; i < count; ++i)
        POP();
    }
    DISPATCH();

    CASE_INST(Property): {
      PropertyName propertyName;
      propertyName.Read(*br);
      s->m_library->RegisterPropertyName(propertyName, false);
      runtime->SetProperty(propertyName.GetId(), propertyName.GetDefaultValue());
    }
    DISPATCH();

    CASE_INST(PushColl): {
      uint32_t count;
      READ(&count);
      Variant collection(CreateCollection());
      for (uint32_t i = 0; i < count; ++i)
      {
        size_t offset = (count - i) * 2;
        if (offset > stack.size())
        {
          ERROR("Collection data error");
          return false;
        }
        size_t index = stack.size() - offset;
        if ((index + 1) > stack.size())
        {
          ERROR("Error in collection data");
          return false;
        }
        Variant key = stack[index];
        if (!key.IsKeyType())
        {
          ERROR("Invalid key type");
          return false;
        }
        Variant value = stack[index + 1];
        collection.GetCollection()->insert(std::make_pair(key, value));
      }
      for (uint32_t i = 0; i < count * 2; ++i)
        stack.pop_back();
      PUSH(collection);
    }
    DISPATCH();

    CASE_INST(PushItr): {
      assert(!stack.empty());
      auto top = stack.size() - 1;
      auto coll = stack[top];
      if (!coll.IsCollection())
      {
        ERROR("Expected collection type");
        return false;
      }
      Variant itr = std::make_pair(coll.GetCollection()->begin(), coll.GetCollection());
      PUSH(itr);
    }
    DISPATCH();

    CASE_INST(PushKeyVal): {
      auto key = POP();
      auto var = POP();
      if (var.IsCollection()) {
        if (!key.IsKeyType()) {
          ERROR("Invalid key type");
          return false;
        }
        auto itr = var.GetCollection()->find(key);
        if (itr == var.GetCollection()->end()) {
          ERROR("Specified key does not exist in collection");
          return false;
        } else {
          PUSH(itr->second);
        }
      } else if (var.IsString()) {
        if (!key.IsInteger() && !s->IsIntegerPair(key)) {
          ERROR("Invalid index type for string");
          return false;
        }
        std::optional<String> optStr;
        if (key.IsInteger())
          optStr = GetUtf8CharByIndex(var.GetString(), key.GetInteger());
        else
          optStr = GetUtf8CharsByRange(var.GetString(), s->GetIntegerPair(key));
        if (!optStr) {
          ERROR("Unable to get string character via index");
          return false;
        }
        PUSH(optStr.value());
      } else {
        ERROR("Expected collection or string type when using brackets");
        return false;
      }
    }
    DISPATCH();

    CASE_INST(PushList): {
      uint32_t count;
      READ(&count);
      Variant collection(CreateCollection());
      if (count > stack.size()) {
        ERROR("Push list error");
        return false;
      }
      for (uint32_t i = 0; i < count; ++i) {
        size_t index = stack.size() - (count - i);
        Variant value = stack[index];
        Variant key(static_cast<int64_t>(i) + 1);
        collection.GetCollection()->insert(std::make_pair(key, value));
      }
      for (uint32_t i = 0; i < count; ++i)
        stack.pop_back();
      PUSH(collection);
    }
    DISPATCH();

    CASE_INST(PushProp): {
      uint64_t id;
      READ(&id);
      auto val = runtime->GetProperty(id);
      PUSH(val);
    }
    DISPATCH();

    CASE_INST(PushTop): {
      assert(!stack.empty());
      auto op = stack[stack.size() - 1];
      PUSH(op);
    }
    DISPATCH();

    CASE_INST(PushVar): {
      RuntimeID id;
      READ(&id);
      auto var = s->GetVariable(id);
      PUSH(var);
    }
    DISPATCH();

    CASE_INST(PushVal): {
      Variant val;
      val.Read(*br);
      PUSH(val);
    }
    DISPATCH();

    CASE_INST(Return): {
      auto val = POP();
      assert(!s->m_execution.empty());
      size_t targetSize = frame->stackTop;
      auto onReturn = frame->onReturn;
      while (!s->m_idIndexData.empty()) {
        if (s->m_idIndexData.back().frameIndex < s->m_execution.size())
          break;
        s->m_idIndexData.pop_back();
      }
      s->m_execution.pop_back();
      assert(!s->m_execution.empty());
      while (stack.size() > targetSize)
        stack.pop_back();
      PUSH(val);
      switch (onReturn) {
        case Script::OnReturn::Finish:
          s->m_finished = true;
          goto __JINX_INTERP_EXIT;
        case Script::OnReturn::Wait:
          if (runUntilFinish)
            goto __JINX_INTERP_NEXT_FRAME;
          else
            goto __JINX_INTERP_EXIT;
        case Script::OnReturn::Continue:
          UPDATE_FRAME();
          // FALLTHROUGH
      }
    }
    DISPATCH();

    CASE_INST(ScopeBegin):
      s->m_scopeStack.push_back(stack.size());
      DISPATCH();

    CASE_INST(ScopeEnd): {
      auto stackTop = s->m_scopeStack.back();
      s->m_scopeStack.pop_back();
      while (stack.size() > stackTop)
        stack.pop_back();
      while (!s->m_idIndexData.empty()) {
        if (s->m_idIndexData.back().index < stackTop)
          break;
        s->m_idIndexData.pop_back();
      }
    }
    DISPATCH();

    CASE_INST(SetIndex): {
      assert(!stack.empty());
      RuntimeID id;
      READ(&id);
      int32_t stackIndex;
      READ(&stackIndex);
      ValueType type;
      br->Read<ValueType, uint8_t>(&type);
      size_t index = stack.size() + stackIndex;
      if (type != ValueType::Any) {
        if (!stack[index].ConvertTo(type)) {
          ERROR("Invalid function parameter cast");
          return false;
        }
      }
      s->SetVariableAtIndex(id, index);
    }
    DISPATCH();

    CASE_INST(SetProp): {
      RuntimeID id;
      READ(&id);
      Variant val = POP();
      runtime->SetProperty(id, val);
    }
    DISPATCH();

    CASE_INST(SetPropKeyVal): {
      uint32_t subscripts;
      READ(&subscripts);
      RuntimeID id;
      READ(&id);
      bool success = runtime->SetProperty(id, [&](Variant &var) -> bool {
        if (var.IsCollection()) {
          auto collection = var.GetCollection();
          Variant val = POP();
          auto pair = s->WalkSubscripts(subscripts, collection);
          if (pair.first == nullptr) {
            ERROR("Could not find property collection");
            return false;
          }
          collection = pair.first;
          Variant key = pair.second;
          (*collection)[key] = val;
        } else if (var.IsString()) {
          Variant val = POP();
          if (!val.IsString()) {
            ERROR("String index operation must be assigned a string");
            return false;
          }
          Variant index = POP();
          if (!index.IsInteger() && !s->IsIntegerPair(index)) {
            ERROR("String index must be an integer or integer pair");
            return false;
          }
          std::optional<String> s2;
          if (index.IsInteger())
            s2 = ReplaceUtf8CharAtIndex(var.GetString(), val.GetString(),
                                       index.GetInteger());
          else
            s2 = ReplaceUtf8CharsAtRange(var.GetString(), val.GetString(),
                                        s->GetIntegerPair(index));
          if (!s2) {
            ERROR("Unable to set string via index");
            return false;
          }
          var = s2.value();
        } else {
          ERROR("Expected collection or string when accessing property using "
                "brackets");
          return false;
        }
        return true;
      });
      if (!success)
        return false;
    }
    DISPATCH();

    CASE_INST(SetVar): {
      RuntimeID id;
      READ(&id);
      Variant val = POP();
      s->SetVariable(id, val);
    }
    DISPATCH();

    CASE_INST(SetVarKeyVal): {
      uint32_t subscripts;
      READ(&subscripts);
      RuntimeID id;
      READ(&id);
      Variant var = s->GetVariable(id);
      if (var.IsCollection()) {
        auto collection = var.GetCollection();
        Variant val = POP();
        auto pair = s->WalkSubscripts(subscripts, collection);
        if (pair.first == nullptr)
          return false;
        collection = pair.first;
        Variant key = pair.second;
        (*collection)[key] = val;
      } else if (var.IsString()) {
        Variant val = POP();
        if (!val.IsString()) {
          ERROR("String index operation must be assigned a string");
          return false;
        }
        Variant index = POP();
        if (!index.IsInteger() && !s->IsIntegerPair(index)) {
          ERROR("String index must be an integer or integer pair");
          return false;
        }
        std::optional<String> s2;
        if (index.IsInteger())
          s2 = ReplaceUtf8CharAtIndex(var.GetString(), val.GetString(),
                                     index.GetInteger());
        else
          s2 = ReplaceUtf8CharsAtRange(var.GetString(), val.GetString(),
                                      s->GetIntegerPair(index));
        if (!s2) {
          ERROR("Unable to set string via index");
          return false;
        }
        s->SetVariable(id, s2.value());
      } else {
        ERROR("Expected collection or string when accessing variable with "
              "brackets");
        return false;
      }
    }
    DISPATCH();

    CASE_INST(Subtract): {
      auto op2 = POP();
      auto op1 = POP();
      auto result = op1 - op2;
      if (result.IsNull())
      {
        ERROR("Invalid variable for subtraction");
        return false;
      }
      PUSH(result);
    }
    DISPATCH();

    CASE_INST(Type):
      PUSH(POP().GetType());
      DISPATCH();

    CASE_INST(Wait):
      if (runUntilFinish) {
        DISPATCH();
      } else {
        goto __JINX_INTERP_EXIT;
      }
  }
__JINX_INTERP_EXIT:

  // Track accumulated script execution time
  auto end = std::chrono::high_resolution_clock::now();
  uint64_t executionTimeNs = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count();
  runtime->AddPerformanceParams(s->m_finished, executionTimeNs, tickInstCount);

  return true;
}

} // namespace Finx::Impl
