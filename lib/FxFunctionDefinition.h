/*>>============================================================================
  The Finx library is distributed under the MIT License (MIT)
    https://opensource.org/licenses/MIT
  See LICENSE or Finx.h for license details.
  Copyright (c) 2016 James Boer.
  Copyright (c) 2022 Weihao Feng.
============================================================================>>*/

#pragma once
#ifndef FX_FUNCTION_DEFINITION_H
#define FX_FUNCTION_DEFINITION_H

#include <utility>

#include "Finx.h"
#include "FxFunctionSignature.h"

namespace Finx::Impl {

class FunctionDefinition {
public:
  FunctionDefinition(const FunctionSignature& signature, BufferPtr bytecode, size_t offset) :
    m_id(signature.GetId()), m_parameterCount(signature.GetParameterCount()), m_bytecode(bytecode),
    m_offset(offset), m_name(signature.GetName()) {}
  FunctionDefinition(const FunctionSignature& signature, FunctionCallback callback) :
    m_id(signature.GetId()), m_parameterCount(signature.GetParameterCount()), m_bytecode(nullptr),
    m_offset(0), m_name(signature.GetName()), m_callback(callback) {}

  [[nodiscard]] size_t GetParameterCount() const { return m_parameterCount; }
  [[nodiscard]] RuntimeID GetId() const { return m_id; }
  [[nodiscard]] const BufferPtr& GetBytecode() const { return m_bytecode; }
  [[nodiscard]] size_t GetOffset() const { return m_offset; }
  [[nodiscard]] FunctionCallback GetCallback() const { return m_callback; }
  [[nodiscard]] const char* GetName() const { return m_name.c_str(); }

  friend class FunctionTable;

private:
  RuntimeID m_id;
  size_t m_parameterCount;
  BufferPtr m_bytecode;
  size_t m_offset;
  String m_name;
  FunctionCallback m_callback;
};

using FunctionDefinitionPtr = std::shared_ptr<FunctionDefinition>;

} // Jinx::Impl

#endif // FX_FUNCTION_DEFINITION_H

