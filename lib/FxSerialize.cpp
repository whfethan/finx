/*>>============================================================================
  The Finx library is distributed under the MIT License (MIT)
    https://opensource.org/licenses/MIT
  See LICENSE or Finx.h for license details.
  Copyright (c) 2016 James Boer.
  Copyright (c) 2022 Weihao Feng.
============================================================================>>*/

#include "FxInternal.h"

namespace Finx::Impl {

// TODO: Decrease from 2 to 1?? Revisit here later.
void BinaryReader::Read(String * val)
{
  uint32_t length = 0;
  m_buffer->Read(&m_pos, &length, sizeof(length));
  if (length < (1024 - 1))
  {
    char buffer[1024];
    m_buffer->Read(&m_pos, buffer, length + 1);
    *val = buffer;
  }
  else
  {
    char * buffer = (char *)MemAllocate(length + 2);
    m_buffer->Read(&m_pos, buffer, length + 1);
    *val = buffer;
    MemFree(buffer, length + 1);
  }
}

void BinaryReader::Read(BufferPtr & val)
{
  uint32_t size;
  m_buffer->Read(&m_pos, &size, sizeof(size));
  val->Reserve(size);
  m_buffer->Read(&m_pos, val, size);
}


void BinaryWriter::Write(const char * val)
{
  uint32_t size = static_cast<uint32_t>(strlen(val));
  m_buffer->Write(&m_pos, &size, sizeof(size));
  m_buffer->Write(&m_pos, val, size + 1);
}

void BinaryWriter::Write(const String & val)
{
  uint32_t size = static_cast<uint32_t>(val.size());
  m_buffer->Write(&m_pos, &size, sizeof(size));
  m_buffer->Write(&m_pos, val.c_str(), size + 1);
}

void BinaryWriter::Write(const BufferPtr & val)
{
  uint32_t size = static_cast<uint32_t>(val->Size());
  m_buffer->Write(&m_pos, &size, sizeof(uint32_t));
  m_buffer->Write(&m_pos, val->Data(0), val->Size());
}

void BinaryWriter::Write(BinaryReader & reader, size_t bytes)
{
  assert((reader.m_pos + bytes) <= reader.m_buffer->Size());
  m_buffer->Write(&m_pos, reader.m_buffer->Data(0) + reader.m_pos, bytes);
  reader.m_pos += bytes;
}

} // namespace Finx::Impl
