/*>>============================================================================
  The Finx library is distributed under the MIT License (MIT)
    https://opensource.org/licenses/MIT
  See LICENSE or Finx.h for license details.
  Copyright (c) 2016 James Boer.
  Copyright (c) 2022 Weihao Feng.
============================================================================>>*/

#pragma once
#ifndef FX_LOGGING_H
#define FX_LOGGING_H


namespace Finx::Impl
{

  void InitializeLogging(const GlobalParams & params);

  bool IsLogSymbolsEnabled();
  bool IsLogBytecodeEnabled();

  void LogWrite(LogLevel level, const char * format, ...);
  void LogWriteLine(LogLevel level, const char * format, ...);

} // namespace Finx::Impl

#endif // FX_LOGGING_H
