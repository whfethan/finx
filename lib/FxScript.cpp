/*>>============================================================================
  The Finx library is distributed under the MIT License (MIT)
    https://opensource.org/licenses/MIT
  See LICENSE or Finx.h for license details.
  Copyright (c) 2016 James Boer.
  Copyright (c) 2022 Weihao Feng.
============================================================================>>*/

#include "FxInternal.h"
#include "FxScript.h"

namespace Finx::Impl
{

  Script::Script(RuntimeIPtr runtime, BufferPtr bytecode, Any userContext, uint32_t appId) :
    m_runtime(runtime),
    m_userContext(userContext)
  {
    // Reserve initial memory
    m_execution.reserve(6);
    m_stack.reserve(32);
    m_scopeStack.reserve(32);
    m_idIndexData.reserve(32);
    m_localFunctions.reserve(8);

    // Create root execution frame
    m_execution.emplace_back(bytecode, "root");

    // Assume default unnamed library unless explicitly overridden
    m_library = m_runtime->GetLibraryInternal("");

    // Read bytecode header
    BytecodeHeader header;
    auto & reader = m_execution.back().reader;
    reader.Read(&header, sizeof(header));
    if (header.signature != BytecodeSignature || header.version != BytecodeVersion || header.dataSize == 0)
    {
      Error("Invalid bytecode");
    }
    else if (header.applicationId != appId)
    {
      Error("Incompatible application ID");
    }

    // Read the script name
    reader.Read(&m_name);
    if (m_name.empty())
      m_name = "(unnamed)";

    // Mark the starting position of the executable bytecode
    m_bytecodeStart = reader.Tell();
  }

  Script::~Script()
  {
    // Clear potential circular references by explicitly destroying collection values
    for (auto & s : m_stack)
    {
      if (s.IsCollection())
      {
        auto c = s.GetCollection();
        for (auto & e : *c)
        {
          e.second.SetNull();
        }
      }
    }

    // Unregister local functions
    for (auto id : m_localFunctions)
      m_runtime->UnregisterFunction(id);
  }

  void Script::Error(const char * message)
  {
    // Set flags to indicate a fatal runtime error
    m_error = true;
    m_finished = true;

    // Try to determine line number in current script execution context
    uint32_t lineNumber = 0;
    auto & reader = m_execution.back().reader;
    auto bytecodePos = reader.Tell();
    reader.Seek(0);
    BytecodeHeader bytecodeHeader;
    reader.Read(&bytecodeHeader, sizeof(bytecodeHeader));
    if (reader.Size() > sizeof(bytecodeHeader) + bytecodeHeader.dataSize)
    {
      // Validate debug info
      reader.Seek(sizeof(bytecodeHeader) + bytecodeHeader.dataSize);
      if (reader.Size() < sizeof(bytecodeHeader) + bytecodeHeader.dataSize + sizeof(DebugHeader))
      {
        LogWriteLine(LogLevel::Error, "Potentially corrupt bytecode debug data");
        return;
      }
      DebugHeader debugHeader;
      reader.Read(&debugHeader, sizeof(debugHeader));
      if (debugHeader.signature != DebugSignature ||
        reader.Size() < sizeof(bytecodeHeader) + bytecodeHeader.dataSize + sizeof(debugHeader) + debugHeader.dataSize)
      {
        LogWriteLine(LogLevel::Error, "Potentially corrupt bytecode debug data");
        return;
      }

      // Read bytecode to line number table
      for (uint32_t i = 0; i < debugHeader.lineEntryCount; ++i)
      {
        DebugLineEntry lineEntry;
        reader.Read(&lineEntry, sizeof(lineEntry));
        if (lineEntry.opcodePosition > bytecodePos)
          break;
        lineNumber = lineEntry.lineNumber;
      }
    }

    // If we have a line number, use it.  Otherwise, just report what we know.
    if (lineNumber)
      LogWriteLine(LogLevel::Error, "Runtime error in script '%s' at line %i: %s", m_name.c_str(), lineNumber, message);
    else
      LogWriteLine(LogLevel::Error, "Runtime error in script '%s': %s", m_name.c_str(), message);
  }

  inline bool Script::Execute(bool runUntilFinish)
  {
    return OptimizedScriptRunner::Execute(this, runUntilFinish);
  }

  RuntimeID Script::FindFunction(LibraryPtr library, const String & name)
  {
    if (library == nullptr)
      library = m_library;
    auto libraryInt = std::static_pointer_cast<Library>(library);
    return libraryInt->FindFunctionSignature(Visibility::Public, name).GetId();
  }

  void Script::CallBytecodeFunction(const FunctionDefinitionPtr & fnDef, OnReturn onReturn)
  {
    m_execution.emplace_back(fnDef);
    m_execution.back().onReturn = onReturn;
    m_execution.back().reader.Seek(fnDef->GetOffset());
    assert(m_stack.size() >= fnDef->GetParameterCount());
    m_execution.back().stackTop = m_stack.size() - fnDef->GetParameterCount();
  }

  CoroutinePtr Script::CallAsyncFunction(RuntimeID id, Parameters params)
  {
    return CreateCoroutine(shared_from_this(), id, params);
  }

  Variant Script::CallFunction(RuntimeID id, Parameters params)
  {
    for (const auto & param : params)
      Push(param);
    return CallFunction(id);
  }

  Variant Script::CallFunction(RuntimeID id)
  {
    FunctionDefinitionPtr functionDef = m_runtime->FindFunction(id);
    if (!functionDef)
    {
      Error("Could not find function definition");
      return false;
    }
    // Check to see if this is a bytecode function
    if (functionDef->GetBytecode())
    {
      CallBytecodeFunction(functionDef, OnReturn::Wait);
      bool finished = m_finished;
      m_finished = false;
      if (!Execute())
        return nullptr;
      m_finished = finished;
      return Pop();
    }
    // Otherwise, call a native function callback
    else if (functionDef->GetCallback())
    {
      return CallNativeFunction(functionDef);
    }
    else
    {
      Error("Error in function definition");
    }
    return nullptr;
  }

  Variant Script::CallNativeFunction(const FunctionDefinitionPtr & fnDef)
  {
    Parameters params;
    size_t numParams = fnDef->GetParameterCount();
    for (size_t i = 0; i < numParams; ++i)
    {
      size_t index = m_stack.size() - (numParams - i);
      const auto & param = m_stack[index];
      params.push_back(param);
    }
    for (size_t i = 0; i < numParams; ++i)
      m_stack.pop_back();
    return fnDef->GetCallback()(shared_from_this(), params);
  }

  std::vector<String, Allocator<String>> Script::GetCallStack() const
  {
    std::vector<String, Allocator<String>> strings;
    for (const auto & frame : m_execution)
      strings.emplace_back(frame.name);
    return strings;
  }

  Variant Script::GetVariable(const String & name) const
  {
    const auto & foldedName = FoldCase(name);
    RuntimeID id = GetVariableId(foldedName.c_str(), foldedName.size(), 1);
    return GetVariable(id);
  }

  Variant Script::GetVariable(RuntimeID id) const
  {
    for (auto ritr = m_idIndexData.rbegin(); ritr != m_idIndexData.rend(); ++ritr)
    {
      if (ritr->id == id)
      {
        if (ritr->index >= m_stack.size())
        {
          LogWriteLine(LogLevel::Error, "Attempted to access stack at invalid index");
          return {};
        }
        return m_stack[ritr->index];
      }
    }
    return {};
  }

  bool Script::IsFinished() const
  {
    return m_finished || m_error;
  }

  bool Script::IsIntegerPair(const Variant & value) const
  {
    if (!value.IsCollection())
      return false;
    auto coll = value.GetCollection();
    if (coll->size() != 2)
      return false;
    const auto & first = coll->begin()->first;
    const auto & second = coll->rbegin()->first;
    if (!first.IsInteger() || !second.IsInteger())
      return false;
    return true;
  }

  std::pair<int64_t, int64_t> Script::GetIntegerPair(const Variant & value) const
  {
    assert(IsIntegerPair(value));
    auto coll = value.GetCollection();
    const auto & first = coll->begin()->second;
    const auto & second = coll->rbegin()->second;
    return { first.GetInteger(), second.GetInteger() };
  }

  Variant Script::Pop()
  {
    if (m_stack.empty())
    {
      Error("Stack underflow");
      return {};
    }
    auto var = m_stack.back();
    m_stack.pop_back();
    return var;
  }

  void Script::Push(const Variant & value)
  {
    m_stack.push_back(value);
  }

  void Script::SetVariable(const String & name, const Variant & value)
  {
    const auto & foldedName = FoldCase(name);
    RuntimeID id = GetVariableId(foldedName.c_str(), foldedName.size(), 1);
    SetVariable(id, value);
  }

  void Script::SetVariable(RuntimeID id, const Variant & value)
  {
    // Search the current frame for the variable
    for (auto ritr = m_idIndexData.rbegin(); ritr != m_idIndexData.rend(); ++ritr)
    {
      if (ritr->frameIndex < m_execution.size())
        break;
      if (ritr->id == id)
      {
        if (ritr->index >= m_stack.size())
        {
          ritr->index = m_stack.size();
          m_stack.push_back(value);
        }
        else
        {
          m_stack[ritr->index] = value;
        }
        return;
      }
    }

    // If we don't find the name, create a new variable on the top of the stack
    m_idIndexData.emplace_back(id, m_stack.size(), m_execution.size());
    m_stack.push_back(value);
  }

  void Script::SetVariableAtIndex(RuntimeID id, size_t index)
  {
    assert(index < m_stack.size());
    for (auto ritr = m_idIndexData.rbegin(); ritr != m_idIndexData.rend(); ++ritr)
    {
      if (ritr->frameIndex < m_execution.size())
        break;
      if (ritr->id == id)
      {
        ritr->index = index;
        return;
      }
    }
    m_idIndexData.emplace_back(id, index, m_execution.size());
  }

  std::pair<CollectionPtr, Variant> Script::WalkSubscripts(uint32_t subscripts, CollectionPtr collection)
  {
    // Walk up through subsequent subscript operators, then pops the keys off the stack and
    // returns the final collection and key pair,
    Variant key;

    // Loop through the number of subscript operations used
    for (uint32_t i = 0; i < subscripts; ++i)
    {
      // Grab the appropriate key in the stack for this subscript
      size_t index = m_stack.size() - (subscripts - i);
      key = m_stack[index];
      if (!key.IsKeyType())
      {
        Error("Invalid key type");
        return {};
      }

      // We only need to retrieve a new collection and key set if
      // this isn't the last operation.
      if (i < (subscripts - 1))
      {
        auto itr = collection->find(key);
        if (itr == collection->end())
        {
          Variant newColl = CreateCollection();
          collection->insert(std::make_pair(key, newColl));
          collection = newColl.GetCollection();
        }
        else if (itr->second.IsCollection())
        {
          collection = itr->second.GetCollection();
        }
        else
        {
          Error("Expected collection when accessing by key");
          return {};
        }
      }
    }

    // Pop keys off the stack
    for (uint32_t i = 0; i < subscripts; ++i)
      Pop();

    // Return the final collection and key pair
    return std::make_pair(collection, key);
  }


} // namespace Finx::Impl

