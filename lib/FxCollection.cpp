/*>>============================================================================
  The Finx library is distributed under the MIT License (MIT)
    https://opensource.org/licenses/MIT
  See LICENSE or Finx.h for license details.
  Copyright (c) 2016 James Boer.
  Copyright (c) 2022 Weihao Feng.
============================================================================>>*/

#include "FxInternal.h"

namespace Finx {

CollectionPtr CreateCollection()
{
  return std::allocate_shared<Collection>(Allocator<Collection>());
}

} // namespace Finx
