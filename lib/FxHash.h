/*>>============================================================================
  The Finx library is distributed under the MIT License (MIT)
    https://opensource.org/licenses/MIT
  See LICENSE or Finx.h for license details.
  Copyright (c) 2016 James Boer.
  Copyright (c) 2022 Weihao Feng.
============================================================================>>*/

#pragma once
#ifndef FX_HASH_H
#define FX_HASH_H


namespace Finx::Impl
{

  uint64_t GetHash(const void * data, size_t len);

} // namespace Finx::Impl

#endif // FX_HASH_H


