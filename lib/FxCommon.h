/*>>============================================================================
  The Finx library is distributed under the MIT License (MIT)
    https://opensource.org/licenses/MIT
  See LICENSE or Finx.h for license details.
  Copyright (c) 2016 James Boer.
  Copyright (c) 2022 Weihao Feng.
============================================================================>>*/

#pragma once
#ifndef FX_COMMON_H
#define FX_COMMON_H

#include <vector>
#include <cstdint>
#include "Finx/_FxMemory.h"
#include "Finx/_FxVariant.h"

namespace Finx::Impl {

const uint32_t LogTabWidth = 4;

// All script opcodes
enum class Opcode {
#define OPCODE(C, _) C,
#include "FxOpcodes.h"
#undef OPCODE
  NumOpcodes,
};

const char* GetOpcodeText(Opcode opcode);

// Symbols
enum class SymbolType {
#define TOKEN(t, _) t,
#include "FxTokens.h"
#undef TOKEN
  NumSymbols,
};

const char* GetSymbolTypeText(SymbolType symbol);

// Check to see if a symbol is a particular category
bool IsConstant(SymbolType symbol);
bool IsOperator(SymbolType symbol);
bool IsKeyword(SymbolType symbol);

// Get the name of a value type
const char* GetValueTypeName(ValueType valueType);

// Visibility type
enum class VisibilityType {
  Local,
  Private,
  Public,
};

const uint32_t BytecodeSignature = 0x8f4a77;
const uint32_t BytecodeVersion = 1;

struct BytecodeHeader {
  BytecodeHeader() : signature(BytecodeSignature), version(BytecodeVersion), dataSize(0) {}

  uint32_t signature: 24;
  uint32_t version: 8;
  uint32_t applicationId{};
  uint32_t instCount{};
  uint32_t dataSize;
};

static_assert(sizeof(BytecodeHeader) == 16, "BytecodeHeader struct is not properly aligned on this platform");

const uint32_t DebugSignature = 0x8f4a79;

struct DebugHeader {
  DebugHeader() : signature(DebugSignature), lineEntryCount(0), dataSize(0) {}
  uint32_t signature;
  uint32_t lineEntryCount;
  uint32_t dataSize;
};

static_assert(sizeof(DebugHeader) == 12, "DebugHeader struct is not properly aligned on this platform");

struct DebugLineEntry {
  uint32_t opcodePosition;
  uint32_t lineNumber;
};

inline const char* StrCopy(char* dest, size_t destBufferSize, const char* source)
{
#if defined(JINX_WINDOWS)
  strncpy_s(dest, destBufferSize, source, ((size_t)-1));
  return dest;
#else
  strncpy(dest, source, destBufferSize);
  return dest;
#endif
}

// Get number of parts in name
size_t GetNamePartCount(const String& name);

// Generate runtime id from unique name information
RuntimeID GetVariableId(const char* name, size_t nameLen, size_t stackDepth);

RuntimeID GetRandomId();
uint32_t MaxInstructions();
bool ErrorOnMaxInstruction();
bool EnableDebugInfo();

// Forward declarations
class Runtime;

using RuntimeIPtr = std::shared_ptr<Runtime>;
using RuntimeWPtr = std::weak_ptr<Runtime>;

struct Symbol {
  Symbol() :
  Symbol(SymbolType::None, 0, 0) {}
  Symbol(SymbolType t, int32_t ln, int32_t cn) :
  type(t), numVal(0), lineNumber(ln), columnNumber(cn) {}
  SymbolType type;
  String text;
  union {
    double numVal;
    int64_t intVal;
    bool boolVal;
  };
  uint32_t lineNumber;
  uint32_t columnNumber;
};

using SymbolList = std::vector<Symbol, Allocator<Symbol>>;
using SymbolListCItr = SymbolList::const_iterator;

// Write symbol text to string
void WriteSymbol(SymbolListCItr symbol, String& output);

// Shared aliases
static const size_t RuntimeArenaSize = 4096;
using SymbolTypeMap = std::map<std::string_view, SymbolType, std::less<std::string_view>, StaticAllocator<std::pair<const std::string_view, SymbolType>, RuntimeArenaSize>>;

} // namespace Finx::Impl

#endif // FX_COMMON_H
