/*>>============================================================================
  The Finx library is distributed under the MIT License (MIT)
    https://opensource.org/licenses/MIT
  See LICENSE or Finx.h for license details.
  Copyright (c) 2016 James Boer.
  Copyright (c) 2022 Weihao Feng.
============================================================================>>*/

#pragma once
#ifndef FX_INTERNAL_H
#define FX_INTERNAL_H

#include "Finx.h"

#ifdef JINX_WINDOWS
#pragma warning(push)
#pragma warning(disable : 4530) // Silence warnings if exceptions are disabled
#endif

#include <cassert>
#include <cstdarg>
#include <mutex>
#include <algorithm>
#include <optional>
#include <memory>
#include <string>
#include <array>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <vector>
#include <random>
#include <cctype>
#include <cinttypes>
#include <string.h>
#include <cstddef>
#include <atomic>
#include <locale>
#include <codecvt>
#include <climits>
#ifdef JINX_USE_FROM_CHARS
#include <charconv>
#else
#include <sstream>
#endif

#ifdef JINX_WINDOWS
#pragma warning(pop)
#endif

#include "FxLogging.h"
#include "FxCommon.h"
#include "FxUnicode.h"
#include "FxUnicodeCaseFolding.h"
#include "FxConversion.h"
#include "FxSerialize.h"
#include "FxPropertyName.h"
#include "FxLexer.h"
#include "FxHash.h"
#include "FxFunctionSignature.h"
#include "FxFunctionDefinition.h"
#include "FxLibrary.h"
#include "FxVariableStackFrame.h"
#include "FxParser.h"
#include "FxScript.h"
#include "FxRuntime.h"
#include "FxLibCore.h"

#endif // FX_INTERNAL_H
