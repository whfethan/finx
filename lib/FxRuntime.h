/*>>============================================================================
  The Finx library is distributed under the MIT License (MIT)
    https://opensource.org/licenses/MIT
  See LICENSE or Finx.h for license details.
  Copyright (c) 2016 James Boer.
  Copyright (c) 2022 Weihao Feng.
============================================================================>>*/

#pragma once
#ifndef FX_RUNTIME_H
#define FX_RUNTIME_H

#include <mutex>
#include "Finx.h"
#include "FxCommon.h"
#include "FxFunctionDefinition.h"
#include "FxLibrary.h"

namespace Finx::Impl
{
  class Parser;
  class OptimizedScriptRunner;

  class Runtime final : public IRuntime, public std::enable_shared_from_this<Runtime>
  {
  public:
    Runtime(uint32_t appId, ExternalLibraryLoaderFn loaderFn, Any loaderArg);
    virtual ~Runtime();

    // IRuntime interface
    BufferPtr Compile(const char * scriptText, String name, std::initializer_list<String> libraries) override;
    ScriptPtr CreateScript(BufferPtr bytecode, Any userContext, bool allowCache = false) override {
      return CreateScriptInternal(bytecode, userContext, allowCache);
    }
    ScriptPtr CreateScript(const char * scriptText, Any userContext, String name, std::initializer_list<String> libraries, bool allowCache = false) override {
      return CreateScriptInternal(scriptText, userContext, name, libraries, allowCache);
    }
    ScriptPtr ExecuteScript(const char * scriptText, Any userContext, String name, std::initializer_list<String> libraries) override;
    LibraryPtr GetLibrary(const String & name) override;
    PerformanceStats GetScriptPerformanceStats(bool resetStats = true) override;
    BufferPtr StripDebugInfo(BufferPtr bytecode) const override;
    ScriptPtr GetCachedScript(const String& name) const override;

    // Internal interface
    BufferPtr Compile(BufferPtr scriptBuffer, String name, std::initializer_list<String> libraries);
    ScriptIPtr CreateScriptInternal(BufferPtr bytecode, Any userContext, bool allowCache);
    ScriptIPtr CreateScriptInternal(const char * scriptText, Any userContext, String name, std::initializer_list<String> libraries, bool allowCache);
    LibraryIPtr GetLibraryInternal(const String & name) { return std::static_pointer_cast<Library>(GetLibrary(name)); }
    FunctionDefinitionPtr FindFunction(RuntimeID id) const;
    bool LibraryExists(const String & name) const;
    void RegisterFunction(const FunctionSignature & signature, const BufferPtr & bytecode, size_t offset);
    void RegisterFunction(const FunctionSignature & signature, FunctionCallback function);
    Variant GetProperty(RuntimeID id) const;
    bool PropertyExists(RuntimeID id) const;
    bool SetProperty(RuntimeID id, std::function<bool(Variant &)> fn);
    void SetProperty(RuntimeID id, const Variant & value);
    void AddPerformanceParams(bool finished, uint64_t timeNs, uint64_t instCount);
    const SymbolTypeMap & GetSymbolTypeMap() const { return m_symbolTypeMap; }
    void UnregisterFunction(RuntimeID id);
    bool LoadExternalScript(ScriptIPtr* out, const String& libPath, std::string* error);
    void CreateLibraryAlias(const String& from, const String& to)
    {
      assert(m_libraryMap.find(from) != m_libraryMap.end());
      m_libraryMap[to] = m_libraryMap[from];
    }

  private:
    using LibraryMap = std::map<String, LibraryIPtr, std::less<String>, StaticAllocator<std::pair<const String, LibraryIPtr>, RuntimeArenaSize>>;
    using ScriptMap = std::map<String, ScriptIPtr, std::less<String>, StaticAllocator<std::pair<const String, ScriptIPtr>, RuntimeArenaSize>>;
    using FunctionMap = std::map<RuntimeID, FunctionDefinitionPtr, std::less<RuntimeID>, StaticAllocator<std::pair<const RuntimeID, FunctionDefinitionPtr>, RuntimeArenaSize>>;
    using PropertyMap = std::map<RuntimeID, Variant, std::less<RuntimeID>, StaticAllocator<std::pair<const RuntimeID, Variant>, RuntimeArenaSize>>;
    void LogBytecode(const Parser & parser) const;
    void LogSymbols(const SymbolList & symbolList) const;

  private:
    StaticArena<RuntimeArenaSize> m_staticArena;
    mutable std::mutex m_libraryMutex;
    LibraryMap m_libraryMap{ m_staticArena };
    mutable std::mutex m_functionMutex;
    FunctionMap m_functionMap{ m_staticArena };
    mutable std::mutex m_propertyMutex;
    PropertyMap m_propertyMap{ m_staticArena };
    mutable std::mutex m_scriptCacheMutex;
    ScriptMap m_cachedScriptMap{ m_staticArena };
    std::mutex m_perfMutex;
    PerformanceStats m_perfStats;
    std::chrono::time_point<std::chrono::high_resolution_clock> m_perfStartTime;
    SymbolTypeMap m_symbolTypeMap{ m_staticArena };
    ExternalLibraryLoaderFn extLibLoaderFn;
    Any extLibLoaderArg;
    uint32_t m_appId;

    friend OptimizedScriptRunner;
  };

} // namespace Finx::Impl

#endif // FX_RUNTIME_H
