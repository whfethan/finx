/*>>============================================================================
  The Finx library is distributed under the MIT License (MIT)
    https://opensource.org/licenses/MIT
  See LICENSE or Finx.h for license details.
  Copyright (c) 2016 James Boer.
  Copyright (c) 2022 Weihao Feng.
============================================================================>>*/

#pragma once
#ifndef FX_UNICODE_CASE_FOLDING_H
#define FX_UNICODE_CASE_FOLDING_H


namespace Finx::Impl
{

  struct CaseFoldingData
  {
    char32_t sourceCodePoint;
    char32_t destCodePoint1;
    char32_t destCodePoint2;
  };

  bool FindCaseFoldingData(char32_t sourceCodePoint, char32_t * destCodePoint1, char32_t * destCodePoint2);

} // namespace Finx::Impl

#endif // FX_UNICODE_CASE_FOLDING_H
