/*>>============================================================================
  The Finx library is distributed under the MIT License (MIT)
    https://opensource.org/licenses/MIT
  See LICENSE or Finx.h for license details.
  Copyright (c) 2016 James Boer.
  Copyright (c) 2022 Weihao Feng.
============================================================================>>*/

#pragma once
#ifndef FX_LIB_CORE_H
#define FX_LIB_CORE_H

namespace Finx::Impl
{
  void RegisterLibCore(RuntimePtr runtime);

} // namespace Finx::Impl




#endif // FX_LIB_CORE_H

