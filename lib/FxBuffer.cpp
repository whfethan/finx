/*>>============================================================================
  The Finx library is distributed under the MIT License (MIT)
    https://opensource.org/licenses/MIT
  See LICENSE or Finx.h for license details.
  Copyright (c) 2016 James Boer.
  Copyright (c) 2022 Weihao Feng.
============================================================================>>*/

#include <algorithm>
#include "Finx/_FxBuffer.h"

namespace Finx {

Buffer::Buffer()
: m_data(nullptr), m_size(0), m_capacity(0)
{
}

Buffer::~Buffer()
{
  MemFree(m_data, m_capacity);
}

size_t Buffer::Capacity() const
{
  return m_capacity;
}

void Buffer::Clear()
{
  m_size = 0;
  if (m_data) m_data[0] = 0;
}

void Buffer::Read(size_t* pos, void* data, size_t bytes)
{
  assert(bytes <= (m_size - *pos));
  size_t bytesToCopy = std::min(bytes, m_size - *pos);
  assert(bytesToCopy);
  assert(bytesToCopy == bytes);
  memcpy(data, m_data + *pos, bytesToCopy);
  *pos += bytes;
}

void Buffer::Read(size_t* pos, BufferPtr& buffer, size_t bytes)
{
  assert(bytes <= (m_size - *pos));
  size_t bytesToCopy = std::min(bytes, m_size - *pos);
  assert(bytesToCopy);
  assert(bytesToCopy == bytes);
  buffer->Reserve(bytesToCopy);
  memcpy(buffer->m_data, m_data + *pos, bytesToCopy);
  *pos += bytes;
  buffer->m_size = bytesToCopy;
}

void Buffer::Reserve(size_t size)
{
  if (size <= m_capacity)
    return;
  m_data = static_cast<uint8_t*>(MemReallocate(m_data, size, m_capacity));
  m_capacity = size;
}

void Buffer::Write(const void* data, size_t bytes)
{
  assert(data && bytes);
  if (m_capacity < bytes)
    Reserve((m_capacity + bytes) + (m_capacity / 2));
  memcpy(m_data, data, bytes);
  m_size = bytes;
}

void Buffer::Write(size_t* pos, const void* data, size_t bytes)
{
  assert(*pos <= m_size);
  assert(data && bytes);
  if (m_capacity < (*pos + bytes))
    Reserve((m_capacity + bytes) + (m_capacity / 2));
  memcpy(m_data + *pos, data, bytes);
  *pos += bytes;
  if (m_size < *pos)
    m_size = *pos;
}

BufferPtr CreateBuffer()
{
  return std::allocate_shared<Buffer>(Allocator<Buffer>());
}

} // namespace Finx
