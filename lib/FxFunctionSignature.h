/*>>============================================================================
  The Finx library is distributed under the MIT License (MIT)
    https://opensource.org/licenses/MIT
  See LICENSE or Finx.h for license details.
  Copyright (c) 2016 James Boer.
  Copyright (c) 2022 Weihao Feng.
============================================================================>>*/

#pragma once
#ifndef FX_FUNCTION_SIGNATURE_H
#define FX_FUNCTION_SIGNATURE_H

#include "Finx/_FxMemory.h"
#include "Finx/_FxVariant.h"
#include "FxCommon.h"

namespace Finx::Impl {

enum class FunctionSignaturePartType {
  Name,
  Parameter,
};

struct FunctionSignaturePart {
  FunctionSignaturePart() = default;
  FunctionSignaturePart(const FunctionSignaturePart& copy);
  FunctionSignaturePart& operator=(const FunctionSignaturePart& copy);

  static const size_t ArenaSize = 128;
  StaticArena<ArenaSize> staticArena;
  FunctionSignaturePartType partType = FunctionSignaturePartType::Name;
  bool optional = false;
  ValueType valueType = ValueType::Any;
  std::vector<String, StaticAllocator<String, ArenaSize>> names{ staticArena };
};

static const size_t FSPBufferSize = 1024;
using FunctionSignaturePartsI = std::vector<FunctionSignaturePart, StaticAllocator<FunctionSignaturePart, FSPBufferSize>>;
using FunctionSignatureParts = std::vector<FunctionSignaturePart, Allocator<FunctionSignaturePart>>;

// Function and member function signature object.
class FunctionSignature {
public:
  FunctionSignature() = default;
  FunctionSignature(VisibilityType visibility, String libraryName, const FunctionSignatureParts& parts);
  FunctionSignature(const FunctionSignature& copy);
  FunctionSignature& operator=(const FunctionSignature& copy);

  // Get unique function id
  [[nodiscard]] RuntimeID GetId() const { return m_id; }

  // Get human-readable name for debug purposes
  [[nodiscard]] String GetName() const;

  // Get signature length
  [[nodiscard]] size_t GetLength() const { return m_parts.size(); }

  // Get visibility level
  [[nodiscard]] VisibilityType GetVisibility() const { return m_visibility; }

  // Get signature parts
  [[nodiscard]] const FunctionSignaturePartsI& GetParts() const { return m_parts; }

  // Is this a valid signature?
  [[nodiscard]] bool IsValid() const { return !m_parts.empty(); }

  // Get a list of parameter parts
  [[nodiscard]] FunctionSignatureParts GetParameters() const;

  // Get number of parameters
  [[nodiscard]] size_t GetParameterCount() const;

  // Serialization
  void Read(BinaryReader& reader);
  void Write(BinaryWriter& writer) const;

private:

  friend bool operator==(const FunctionSignature& left, const FunctionSignature& right);

  // Static memory arena for fast allocations
  StaticArena<FSPBufferSize> m_staticArena;

  // Unique id
  RuntimeID m_id = 0;

  // Visibility level
  VisibilityType m_visibility = VisibilityType::Local;

  // Library name
  String m_libraryName;

  // Each signature is made up of any number of parts representing either part
  // of the function name or a variable placeholder.
  FunctionSignaturePartsI m_parts {m_staticArena};
};

bool operator==(const FunctionSignaturePart& left, const FunctionSignaturePart& right);
bool operator==(const FunctionSignature& left, const FunctionSignature& right);

using FunctionList = std::vector<FunctionSignature, Allocator<FunctionSignature>>;
using FunctionPtrList = std::vector<const FunctionSignature*, Allocator<const FunctionSignature*>>;

} // namespace Finx::Impl

#endif // FX_FUNCTION_SIGNATURE_H

