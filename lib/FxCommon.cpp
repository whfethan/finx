/*>>============================================================================
  The Finx library is distributed under the MIT License (MIT)
    https://opensource.org/licenses/MIT
  See LICENSE or Finx.h for license details.
  Copyright (c) 2016 James Boer.
  Copyright (c) 2022 Weihao Feng.
============================================================================>>*/

#include "FxInternal.h"

namespace Finx {
namespace Impl {

struct HashData {
  char hs[16];
  uint64_t h1;
  uint64_t h2;
};

struct CommonData {
  static inline GlobalParams globalParams;
  static inline std::atomic<uint64_t> uniqueId = {0};
  static inline const char* opcodeName[] = {
#define OPCODE(_, STR) STR,
#include "FxOpcodes.h"
#undef OPCODE
  };

  static_assert(std::size(opcodeName) == static_cast<size_t>(Opcode::NumOpcodes), "Opcode descriptions don't match enum count");

  static inline const char* symbolTypeName[] = {
#define TOKEN(_, str) str,
#include "FxTokens.h"
#undef TOKEN
  };

  static_assert(std::size(symbolTypeName) == static_cast<size_t>(SymbolType::NumSymbols), "SymbolType descriptions don't match enum count");

  static inline const char* valueTypeName[] = {
    "null",
    "number",
    "integer",
    "boolean",
    "string",
    "collection",
    "collectionitr",
    "function",
    "coroutine",
    "userobject",
    "buffer",
    "guid",
    "valtype",
    "any",
  };

  static_assert(std::size(valueTypeName) == (static_cast<size_t>(ValueType::NumValueTypes) + 1), "ValueType names don't match enum count");
};

const char* GetOpcodeText(Opcode opcode)
{
  return CommonData::opcodeName[static_cast<size_t>(opcode)];
}

const char* GetSymbolTypeText(SymbolType symbol)
{
  assert(static_cast<int>(symbol) < static_cast<int>(SymbolType::NumSymbols));
  return CommonData::symbolTypeName[static_cast<size_t>(symbol)];
}

bool IsConstant(SymbolType symbol)
{
  assert(static_cast<int>(symbol) < static_cast<int>(SymbolType::NumSymbols));
  return (static_cast<int>(symbol) > static_cast<int>(SymbolType::NameValue)) && (static_cast<int>(symbol) < static_cast<int>(SymbolType::ForwardSlash));
}

bool IsOperator(SymbolType symbol)
{
  assert(static_cast<int>(symbol) < static_cast<int>(SymbolType::NumSymbols));
  return (static_cast<int>(symbol) >= static_cast<int>(SymbolType::ForwardSlash)) && (static_cast<int>(symbol) < static_cast<int>(SymbolType::And));
}

const char* GetValueTypeName(ValueType valueType)
{
  assert(static_cast<int>(valueType) <= static_cast<int>(ValueType::NumValueTypes));
  return CommonData::valueTypeName[static_cast<size_t>(valueType)];
}

bool IsKeyword(SymbolType symbol)
{
  assert(static_cast<int>(symbol) < static_cast<int>(SymbolType::NumSymbols));
  return static_cast<int>(symbol) >= static_cast<int>(SymbolType::And);
}

size_t GetNamePartCount(const String& name)
{
  size_t parts = 1;
  for (auto itr = name.begin(); itr != name.end(); ++itr) {
    if (*itr == ' ')
      ++parts;
  }
  return parts;
}

RuntimeID GetVariableId(const char* name, size_t nameLen, size_t stackDepth)
{
  RuntimeID id = GetHash(name, nameLen);
  id += static_cast<RuntimeID>(stackDepth);
  return id;
}

RuntimeID GetRandomId()
{
  // Create hash source of current time, a unique id, and a string
  HashData hd{};
  memset(&hd, 0, sizeof(hd));
  StrCopy(hd.hs, 16, "0@@@@UniqueName");
  hd.h1 = std::chrono::high_resolution_clock::time_point().time_since_epoch().count();
  hd.h2 = CommonData::uniqueId++;

  // Return a new random Id from unique hash source
  return GetHash(&hd, sizeof(hd));
}

uint32_t MaxInstructions()
{
  return CommonData::globalParams.maxInstructions;
}

bool ErrorOnMaxInstruction()
{
  return CommonData::globalParams.errorOnMaxInstructions;
}

bool EnableDebugInfo()
{
  return CommonData::globalParams.enableDebugInfo;
}

void WriteSymbol(SymbolListCItr symbol, String& output)
{
  char buffer[768];

  // Write to the output string based on the symbol type
  switch (symbol->type) {
    case SymbolType::None:
      snprintf(buffer, std::size(buffer), "(None) ");
      break;
    case SymbolType::Invalid:
      snprintf(buffer, std::size(buffer), "(Invalid) ");
      break;
    case SymbolType::NewLine:
      snprintf(buffer, std::size(buffer), "\n");
      break;
    case SymbolType::NameValue:
      // Display names with spaces as surrounded by single quotes to help delineate them
      // from surrounding symbols.
      if (strstr(String(symbol->text).c_str(), " "))
        snprintf(buffer, std::size(buffer), "'%s' ", symbol->text.c_str());
      else
        snprintf(buffer, std::size(buffer), "%s ", symbol->text.c_str());
      break;
    case SymbolType::StringValue:
      snprintf(buffer, std::size(buffer), "\"%s\" ", symbol->text.c_str());
      break;
    case SymbolType::NumberValue:
      snprintf(buffer, std::size(buffer), "%f ", symbol->numVal);
      break;
    case SymbolType::IntegerValue:
      snprintf(buffer, std::size(buffer), "%" PRId64 " ", static_cast<int64_t>(symbol->intVal));
      break;
    case SymbolType::BooleanValue:
      snprintf(buffer, std::size(buffer), "%s ", symbol->boolVal ? "true" : "false");
      break;
    default:
      snprintf(buffer, std::size(buffer), "%s ", GetSymbolTypeText(symbol->type));
      break;
  };
  output = buffer;
}


} // namespace Impl

String GetVersionString()
{
  char buffer[32];
  snprintf(buffer, std::size(buffer), "%i.%i.%i", Finx::MajorVersion, Finx::MinorVersion, Finx::PatchNumber);
  return buffer;
}

void Initialize(const GlobalParams& params)
{
  Impl::CommonData::globalParams = params;
  InitializeMemory(params);
  Impl::InitializeLogging(params);
}

} // namespace Finx

